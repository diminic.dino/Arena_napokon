using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : MonoBehaviour
{
    Vector2 startPosition;

    void Update()
    {
        var translation = Vector3.zero;

        if (Mouse.current.leftButton.isPressed || Mouse.current.rightButton.isPressed)
        {
            var position = Mouse.current.position.ReadValue();

            if (startPosition != default)
            {
                var delta = (position - startPosition).normalized;

                if (Mouse.current.leftButton.isPressed)
                {
                    translation.x = delta.x;
                    translation.y = delta.y;
                }

                if (Mouse.current.rightButton.isPressed)
                {
                    var forward = Vector3.forward + new Vector3(delta.normalized.x, delta.normalized.y, 0) * 3 * Time.deltaTime;
                    transform.LookAt(transform.TransformPoint(forward));

                }
            }

            if (startPosition != position)
            {
                startPosition = position;
            }

        }
        else
        {
            startPosition = default;
        }

        var zoom = Mouse.current.scroll.ReadValue().normalized;
        if (zoom != default)
        {
            translation.z = zoom.y * 10;
        }

        if (translation != default)
        {
            transform.Translate(translation * 20 * Time.deltaTime);
        }
    }
}
