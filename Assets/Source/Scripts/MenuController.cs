using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Canvas))]
public class MenuController : MonoBehaviour, IPointerClickHandler
{
    Canvas canvas;
    InputActions inputActions;

    void Awake()
    {
        canvas = GetComponent<Canvas>();
        inputActions = new InputActions();
    }

    void OnEnable()
    {
        inputActions.Enable();
        canvas.enabled = false;
        inputActions.General.ToggleMenu.performed += OnToggleMenu;
    }

    void OnDisable()
    {
        inputActions.Disable();
        inputActions.General.ToggleMenu.performed -= OnToggleMenu;
    }

    void OnToggleMenu(InputAction.CallbackContext context)
    {
        canvas.enabled = !canvas.enabled;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        canvas.enabled = false;
    }
}
