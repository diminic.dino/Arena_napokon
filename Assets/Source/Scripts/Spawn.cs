using UnityEngine;
using UnityEngine.InputSystem;

public class Spawn : MonoBehaviour
{
    InputActions inputActions;

    void Awake()
    {
        inputActions = new InputActions();
    }

    void Start()
    {
        GetComponent<MeshRenderer>().enabled = false;
        Respawn();
    }

    void OnEnable()
    {
        inputActions.Enable();
        inputActions.General.Respawn.performed += OnRespawn;
    }

    void OnDisable()
    {
        inputActions.Disable();
        inputActions.General.Respawn.performed -= OnRespawn;
    }

    void OnRespawn(InputAction.CallbackContext context)
    {
        Respawn();
    }

    void Respawn()
    {
        Camera.main.transform.position = transform.position;
        Camera.main.transform.rotation = transform.rotation;
    }
}
